// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawnBase.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "SnakeBase.h"
#include "Food.h"
#include "SpeedBonus.h"
#include "Components/InputComponent.h"

int GetRandom(int p_min, int p_max)
{
	int l_result;
	l_result = p_min + rand() % (p_max - p_min + 1);

	return l_result;
}

FVector GetRandomFieldPoint(int p_fieldSize)
{
	int l_max = p_fieldSize / 2;
	int l_min = -l_max;
	FVector l_result = FVector(GetRandom(l_min, l_max), GetRandom(l_min, l_max), 0);

	return l_result;
}

// Sets default values
APlayerPawnBase::APlayerPawnBase()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	InitMovementSpeed = 0;
	PrimaryActorTick.bCanEverTick = true;
	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));
	RootComponent = PawnCamera;
}

// Called when the game starts or when spawned
void APlayerPawnBase::BeginPlay()
{
	playerScore = 0;
	scoreDestination = 0;
	srand(time(NULL));
	Super::BeginPlay();
	SetActorRotation(FRotator(-90, 0, 0));
	SetLevel(1);
}

// Called when the game starts or when spawned
void APlayerPawnBase::SetLevel(int pLevel)
{
	playerLevel = pLevel;
	scoreDestination = 50 * playerLevel;

	if (playerLevel > 1)
	{
		if (IsValid(SnakeActor))
		{
			SnakeActor->SpeedBoost(5);
		}
	}
}

// Called every frame
void APlayerPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// ������� ������.
	if (!IsValid(SnakeActor))
	{
		CreateSnakeActor();
	}

	// ������� ��� \ ������.
	if (!IsValid(FoodActor) && !IsValid(SpeedBonusActor))
	{
		if ((playerScore > 0) && (playerScore % 50) == 0)
		{
			CreateSpeedBonusActor();
		}
		else
		{
			CreateFoodActor();
		}
	}
}

// Called to bind functionality to input
void APlayerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAxis("Vertical", this, &APlayerPawnBase::HandlePlayerVerticalInput);
	PlayerInputComponent->BindAxis("Horizontal", this, &APlayerPawnBase::HandlePlayerHorizontalInput);
}

// ��������� �������� ������.
void APlayerPawnBase::CreateSnakeActor()
{
	SnakeActor = GetWorld()->SpawnActor<ASnakeBase>(SnakeActorClass, FTransform());
	SnakeActor->SetSpeed(InitMovementSpeed);
	SnakeActor->PlayerOwner = this;
}

// ��������� �������� ���.
void APlayerPawnBase::CreateFoodActor()
{
  FVector foodPosition = GetRandomFieldPoint(FieldSize);
  FoodActor = GetWorld()->SpawnActor<AFood>(FoodActorClass, FTransform(foodPosition));
}

// ��������� �������� ������ ��������.
void APlayerPawnBase::CreateSpeedBonusActor()
{
	FVector speedBonusPosition = GetRandomFieldPoint(FieldSize);
	SpeedBonusActor = GetWorld()->SpawnActor<ASpeedBonus>(SpeedBonusClass, FTransform(speedBonusPosition));
}

void APlayerPawnBase::addScore(int scoreDelta)
{
	playerScore = playerScore + scoreDelta;
}

// ������ ������.
void APlayerPawnBase::Die()
{
	// ��������� �����.
	addScore(-playerScore);
	scoreDestination = 0;
	// ��������� ������ 1.
	SetLevel(1);
}

// �������� ����-�� ���������.
void APlayerPawnBase::Eat()
{
	// ���������� ������� �����.
	addScore(10);

	// ��� ���������� �������� ����� ����� ��������� ���������� ������.
	if (playerScore >= scoreDestination)
	{
		SetLevel(playerLevel + 1);
    }
}

/* ��������� ������ ������������� ����������.
* @param value - ��������� �������� ������ ������������� ����������� (1 -�����, -1 -����).
*/
void APlayerPawnBase::HandlePlayerVerticalInput(float value)
{
	if (IsValid(SnakeActor))
	{
		if (value > 0 && SnakeActor->LastMoveDirection != EMovementDirection::DOWN)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::UP;
		}
		else if (value < 0 && SnakeActor->LastMoveDirection != EMovementDirection::UP)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::DOWN;
		}
	}
}

/* ��������� ������ ��������������� ����������.
* @param value - ��������� �������� ������ ��������������� ����������� (1 -������, -1 -�����).
*/
void APlayerPawnBase::HandlePlayerHorizontalInput(float value)
{
	if (IsValid(SnakeActor))
	{
		if (value > 0 && SnakeActor->LastMoveDirection != EMovementDirection::RIGHT)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::LEFT;
		}
		else if (value < 0 && SnakeActor->LastMoveDirection != EMovementDirection::LEFT)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::RIGHT;
		}
	}
}