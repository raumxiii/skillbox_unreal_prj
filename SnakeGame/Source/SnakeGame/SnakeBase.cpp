// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "PlayerPawnBase.h"
#include "Interactable.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f; //�������� ������� �������� ��-���������.
	MovementSpeed = 0; //�������� �������� ��-���������.
	LastMoveDirection = EMovementDirection::DOWN; //����������� �������� ��-���������.

	SuperSpeedDuration = 0;
	MegaSlowSpeedDuration = 0;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed); //��������� ������� ������� Tick � ��������.
	AddSnakeElement(5); //�������� ������ ��� ������� ����.
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move(); //��������� ��������.
	if (MegaSlowSpeedDuration > 0)
	{
		MegaSlowSpeedDuration --;
		if (MegaSlowSpeedDuration == 0)
		{
			this->SetSpeed(MovementSpeed);
		}
	}
	if (SuperSpeedDuration > 0)
	{
		SuperSpeedDuration--;
		if(SuperSpeedDuration == 0)
		{
			this->SetSpeed(MovementSpeed);
		}
	}
}

/* ���������� ��������� ������.
 * @param ElementNum - ����� ����������� ���������.
 **/
void ASnakeBase::AddSnakeElement(int ElementNum)
{
	for (int i = 0; i < ElementNum; i++)
	{
		FVector NewLocation(SnakeElements.Num() * ElementSize, 0, 0); // ����������� ���������� ������ ��������.
		FTransform NewTransform(NewLocation);
		ASnakeElementBase* NewSnakeElement = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
	    int32 ElementIndex =  SnakeElements.Add(NewSnakeElement); // ������������ ������ ��������. ��������� ������� ��������.
		NewSnakeElement->SnakeOwner = this;
		// ��������� ������� �������� - ������.
		if (ElementIndex == 0)
		{
			NewSnakeElement->SetFirstElementType();
		}
		else
		{
			NewSnakeElement->MeshComponent->SetVisibility(false);
		}
	};
}

/* ���������� ����������� ������.
 **/
void ASnakeBase::Move()
{
	FVector MovementVector(ForceInitToZero); // �������� ����������� �������.

	// ������������ ������� ����������� � ����������� �� ����������� �������� ������.
	switch(LastMoveDirection)
	{
	    case EMovementDirection::UP:
		    MovementVector.X += ElementSize;
			break;
		case EMovementDirection::DOWN:
		    MovementVector.X -= ElementSize;
		    break;
		case EMovementDirection::LEFT:
			MovementVector.Y += ElementSize;
			break;
		case EMovementDirection::RIGHT:
			MovementVector.Y -= ElementSize;
			break;
	}

	SnakeElements[0]->ToggleCollision();

	// ����������� ������� �������� ������ �� ����� �����������.
	for (int i = SnakeElements.Num() - 1; i > 0; i--)
	{
		auto CurrElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrElement->SetActorLocation(PrevLocation);
		if (!CurrElement->MeshComponent->IsVisible())
		{
			CurrElement->MeshComponent->SetVisibility(true);
		};
	}

	// �������� ��������� �������� �������� ������ �������.
	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();
}

// ��������� ������� ������������ � �������.
void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElementIndex;
		SnakeElements.Find(OverlappedElement, ElementIndex);
		bool bIsFirst = ElementIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if (InteractableInterface)
		{
			InteractableInterface->Interact(this, bIsFirst);
		}
	}
}

// �������� ��������� ������ � ����������� � �������� ���������, �������� ����� ������.
void ASnakeBase::Erase()
{
	// ����������� ���� ��������� ������
	for (int i = 0; i < this->SnakeElements.Num(); i++)
	{
		auto CurrElement = this->SnakeElements[i];
		if (CurrElement)
		{
			CurrElement->MeshComponent->SetVisibility(false);
			CurrElement->Destroy();
		}
	}
	this->Destroy();
}

// �������� ������� ������ � ������ ������ � ������� � ������ ������� ������.
void ASnakeBase::Die()
{
	PlayerOwner->Die();
	this->Erase();
}

// �������� ������� ������ � �������� ��� � ���������� ����� ������.
void ASnakeBase::EatFood()
{
	// �������� ����-�� ���������.
	PlayerOwner->Eat();
	// ���������� ����� ������.
	this->AddSnakeElement();
}

// ������ �������� � ��������� ��������� ���� ��� �������� ����� ������.
void ASnakeBase::SetSpeed(int p_value)
{
	MovementSpeed = p_value;
	if (MovementSpeed > 100)
	{
		MovementSpeed = 100;
	}

	if (SuperSpeedDuration > 0)
	{
		MovementSpeedForTic = 0.1f;
	}
	else if(MegaSlowSpeedDuration > 0)
	{
		MovementSpeedForTic = 1.1f;
	}
	else
	{
		MovementSpeedForTic = 1.1f - float(MovementSpeed) / 100;
	}

	SetActorTickInterval(MovementSpeedForTic);
}

// ������������ ��������� ������.
void ASnakeBase::SpeedBoost(int p_value)
{
	SetSpeed(MovementSpeed + p_value);
}

// ��������� �����-��������.
void ASnakeBase::SetSuperSpeed()
{
	// �������� ����-�� ���������.
	PlayerOwner->Eat();
	// ��������� �����-��������.
	SuperSpeedDuration = 100;
	MegaSlowSpeedDuration = 0;
	this->SetSpeed(MovementSpeed);
}

// ��������� ����-�������.
void ASnakeBase::SetMegaSlowSpeed()
{
	// �������� ����-�� ���������.
   PlayerOwner->Eat();
   // ��������� ����-�������.
   MegaSlowSpeedDuration = 20;
   SuperSpeedDuration = 0;
   this->SetSpeed(MovementSpeed);
}

ASnakeBase::~ASnakeBase()
{
	
}
