// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SnakeBase.generated.h"

class ASnakeElementBase;
class APlayerPawnBase;

UENUM()
enum class EMovementDirection
{
	UP,
	DOWN,
	LEFT,
	RIGHT
};

UCLASS()
class SNAKEGAME_API ASnakeBase : public AActor
{
	GENERATED_BODY()

public:	
	// Sets default values for this actor's properties
	ASnakeBase();

	// �������� - ��� �������� ��� ����������.
	UPROPERTY(EditDefaultsOnly)
	  TSubclassOf<ASnakeElementBase> SnakeElementClass;

	// ������ ���������.
	UPROPERTY(EditDefaultsOnly)
	  float ElementSize;

	// �������� �������� ������.
	UPROPERTY(BlueprintReadOnly)
	  int MovementSpeed;

	UPROPERTY(BlueprintReadOnly)
		float MovementSpeedForTic;

	// ������ ��������� ������.
	UPROPERTY()
		TArray<ASnakeElementBase*> SnakeElements;

	// ������� ����������� ������.
	UPROPERTY()
		EMovementDirection LastMoveDirection;

	UPROPERTY()
		APlayerPawnBase* PlayerOwner;

	UPROPERTY(BlueprintReadOnly)
		int SuperSpeedDuration;
	UPROPERTY(BlueprintReadOnly)
		int MegaSlowSpeedDuration;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// ��������� ��� ���������� ���������.
	UFUNCTION(BlueprintCallable)
	void AddSnakeElement(int ElementNum = 1);

	// ��������� ����������� ������.
	UFUNCTION(BlueprintCallable)
	void Move();

	// ��������� ������� ������������ � �������.
	UFUNCTION()
	void SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other);

	// �������� ��������� ������ � ����������� � �������� ���������, �������� ����� ������.
	void Erase();

	// �������� ������� ������ � ������ ������ � ������� � ������ ������� ������.
	void Die();

	// �������� ������� ������ � �������� ��� � ���������� ����� ������.
	void EatFood();

	// ������ �������� � ��������� ��������� ���� ��� �������� ����� ������.
	void SetSpeed(int p_value);

	// ������������ ��������� ������.
	void SpeedBoost(int p_value);

	// ��������� �����-��������.
	void SetSuperSpeed();

	// ��������� ����-�������.
	void SetMegaSlowSpeed();

private:
	~ASnakeBase();
};
