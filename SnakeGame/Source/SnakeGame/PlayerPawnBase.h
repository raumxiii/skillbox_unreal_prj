// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "PlayerPawnBase.generated.h"

class UCameraComponent;
class ASnakeBase;
class AFood;
class ASpeedBonus;

UCLASS()
class SNAKEGAME_API APlayerPawnBase : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APlayerPawnBase();

    UPROPERTY(BlueprintReadWrite)
		UCameraComponent* PawnCamera;

	// ��������� ��������� ������.
	UPROPERTY(EditDefaultsOnly)
		int FieldSize;

	// ��������� ��������� ������.
	UPROPERTY(EditDefaultsOnly)
		int InitMovementSpeed;

	// ���������� ��� ������ ������.
	UPROPERTY(BlueprintReadWrite)
		ASnakeBase* SnakeActor;
    UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ASnakeBase> SnakeActorClass;

	// ���������� ��� ���.
	UPROPERTY(BlueprintReadWrite)
		AFood* FoodActor;
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AFood> FoodActorClass;

	// ���������� ��� ������ ��������.
	UPROPERTY(BlueprintReadWrite)
		ASpeedBonus* SpeedBonusActor;
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ASpeedBonus> SpeedBonusClass;

	// ���������� ��������� ������.
	UPROPERTY(BlueprintReadWrite)
		int playerLevel;
	UPROPERTY(BlueprintReadWrite)
		int playerScore;
	UPROPERTY(BlueprintReadWrite)
		int scoreDestination;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	void SetLevel(int pLevel);

	// ��������� �������� ������.
	void CreateSnakeActor();

	// ��������� �������� ���.
	void CreateFoodActor();

	// ��������� �������� ������ ��������.
	void CreateSpeedBonusActor();

	// ������ ������.
	void Die();

	// �������� ����-�� ���������.
	void Eat();

	// ��������� ��������� ������ ����������.
	UFUNCTION()
	  void HandlePlayerVerticalInput(float value);
	UFUNCTION()
	  void HandlePlayerHorizontalInput(float value);

private:
	void addScore(int scoreDelta);
};
