// Fill out your copyright notice in the Description page of Project Settings.


#include "SpeedBonus.h"
#include "SnakeBase.h"

// Sets default values
ASpeedBonus::ASpeedBonus()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	if (rand() % 2 == 0)
	{
		bIsSuperSpeed = true;
	}
	else
	{
		bIsSuperSpeed = false;
	}
}

// Called when the game starts or when spawned
void ASpeedBonus::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASpeedBonus::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// ������������ �� �������.
void ASpeedBonus::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			if (bIsSuperSpeed) // ���� �����-��������, ��������� �����-�������� �� ������.
			{
				Snake->SetSuperSpeed();
		    }
			else // ����� ��������� ����-������� �� ������.
			{
				Snake->SetMegaSlowSpeed();
			}
			// ����������� �������� ��������.
			this->Destroy();
		}
	}
}