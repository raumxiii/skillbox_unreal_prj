#include <string>
#include <cstring>
#include <ctime>
#include "vector_class.cpp"
#include "stack_class.cpp"
#include "own_stack_class.cpp"
#include "t_own_stack_class.cpp"
#include "dog_class.cpp"
#include "cat_class.cpp"
#include "cow_class.cpp"

/**
 * ������� ���������� ������� ����� ����������.
 * param p_val1. ������ �������� ��������.
 * param p_val2. ������ �������� ��������.
 * return. ������� ����� ���������� �������.
 */
int get_square_of_sum(int p_val1, int p_val2)
{
	// ���������� ����������.
	int l_result;

	// ���������� �������� ����� ���������� �������
	l_result = p_val1 + p_val2;
	l_result = l_result * l_result;

	// ������� �������� ����� ���������� �������
	return l_result;
}

/**
 * ��������� ������� � ��������� ������ ������� ����� ���� ����� �����.
 */
void homework_13()
{
    // ���������� ����������.
    int l_val1, l_val2;

	// ������ ������� �����.
	std::cout << "Please, enter first int number: ";
	std::cin >> l_val1;

	// ������ ������� �����.
	std::cout << "Please, enter second int number: ";
	std::cin >> l_val2;

	// ����� � ������� �������� ����� �����.
	std::cout << "Your result is: " << get_square_of_sum(l_val1, l_val2) << "\n";
}

/**
 * ��������� ����������� ���� ������, ������� �� ����� �� �����, ������ � ��������� �������.
 */
void homework_14_strings()
{
	// ���������� ����������.
	std::string l_string;
	int l_string_length;

	// ������ ������.
	std::cout << "Please, enter your string: ";
	std::getline(std::cin, l_string);

	// ���������� ����� ������.
	l_string_length = l_string.length();

	// ����� ����������� �� �����.
	std::cout << "\n";
	std::cout << "The Length of your string is: " << l_string_length << "\n";
	std::cout << "The First Symbol is: " << l_string.substr(0, 1) << "; The Last Symbol is: " << l_string.substr(l_string_length - 1, 1) << ".\n";
}

/**
 * ��������� ������� � ������� ������\�������� ����� �� 0 �� ���������.
 * param p_max_num. ������������ �����.
 * param p_even_number. ���� ��������\����������.
 */
void print_even_number_list(int p_max_num, bool p_even_number = false)
{
	// ���������� ����������.
	int l_sign = 1; //���� ���������� �����.

	if (p_max_num < 0) l_sign = -1; //������������ ����� ��� ������������� �����.

	// ���� �� ���� ������ �� 0 �� ���������� �������� ������������.
	for (int i = 0; i <= abs(p_max_num); i++)
	{ 
		// 0 ��������� ������, ��������\���������� ������������ �������� �� ������� �� 2.
		if (i == 0 || (p_even_number && i % 2 == 0) || (not p_even_number && i % 2 == 1))
		  std::cout << (l_sign * i) << "\n";
	}
}

/**
 * ��������� ����������� � ������� ������������ ����� � ���� ��������. ������� � ������� ������\�������� ����� �� 0 �� ���������.
 */
void homework_15_loops()
{
	// ���������� ����������.
	int l_number;
	char l_flag;

	// ������ �����.
	std::cout << "Please, enter Your number: ";
	std::cin >> l_number;

	// ������ ����� ��������.
	std::cout << "Do You need only even numbers? (Y/N): ";
	std::cin >> l_flag;

	// ����� � ������� ������\�������� �����.
	print_even_number_list(l_number, toupper(l_flag) == 'Y');
}

/**
 * ������� � ������� � ������� ��������� ������. ������� ����� ���������.
 * param p_row_cnt. ����� ����� �������.
 * param p_col_cnt. ����� �������� �������.
 * param p_time_divisor. �������� ��� ������� ����������� ������.
 */
void create_n_print_array(int p_row_cnt, int p_col_cnt, int p_time_divisor)
{
	// ���������� ����������.
	int** l_array = new int* [p_row_cnt]; // ������������� ����� ������������� �������.
	std::time_t l_sysdate = std::time(nullptr); // ����������� �������� ������� � ��������.
	struct tm l_tm; // ��������� ��� �������� ����.
	int l_curr_day; // ����� ��� � ����.
	int l_print_row; // ����� ������ ��� ����������� ����� ���������.
	int l_row_sum = 0; // ����� ��������� ������.

	// �������������� �������� ��������� ���� � �������� � �������� ����.
	localtime_s(&l_tm, &l_sysdate);

	// ����������� ������ ��� ���������� ����� ���������.
	l_curr_day = l_tm.tm_mday;
	l_print_row = l_curr_day % p_time_divisor;

	// ���� �� ������� �������.
	for (int c = 0; c < p_row_cnt; c++)
	{
		l_array[c] = new int[p_col_cnt]; // ������������� ������������ ����� ������.
		// ���� �� ��������� � ������.
		for (int k = 0; k < p_col_cnt; k ++)
		{
			// ���������� �������� ����������� ��������.
			l_array[c][k] = c + k;

			// ����� �������� ����������� ��������.
			std::cout << l_array[c][k] << " ";

			// ���� ������ �������������� ����������� ������, ��������� ��������.
			if (c == l_print_row) l_row_sum = l_row_sum + l_array[c][k];
		}
		std::cout << "\n";
	}

	// ����������� �������� ���.
	std::cout << "\n";
	std::cout << "The current day is " << l_curr_day;
	std::cout << "\n";

	// ����������� ����� ��������� � ������ ������������� ������.
	std::cout << "\n";
	std::cout << "Sum of " << l_print_row << "-row is: " << l_row_sum;
	std::cout << "\n";
}

/** 
 * ����������� ����� �����, �������� ������� � �������� ��� ����������� ������ ��� �������� �����.
 * ������� � ������� � ������� ��������� ������. ������� ����� ���������.
 */
void homework_16_array()
{
	// ���������� ����������.
	int l_row_cnt, l_col_cnt, l_divisor;

	// ������ ���������� �����.
	std::cout << "Please, enter number of strings: ";
	std::cin >> l_row_cnt;

	// ������ ���������� ��������.
	std::cout << "Please, enter number of columns: ";
	std::cin >> l_col_cnt;

	// ������ �������� ����������� ������ ��� ������ ����� �������.
	std::cout << "Please, enter divisor for string defination: ";
	std::cin >> l_divisor;

	// �������� � ����������� ������������� �������. ����� ����� ���������.
	create_n_print_array(l_row_cnt, l_col_cnt, l_divisor);
}

/**
 * ��������� ��������� ������. ����� � ������� ����� �������.
 */
void homework_17_vector_class()
{
	// ���������� ����������.
	vector_class l_vector;
	float l_x, l_y, l_z;

	// ������ ��������� �������.
	std::cout << "Please, enter X-coordinate: ";
	std::cin >> l_x;
	std::cout << "Please, enter Y-coordinate: ";
	std::cin >> l_y;
	std::cout << "Please, enter Z-coordinate: ";
	std::cin >> l_z;

	// ��������� ��������� �������.
	l_vector.set_point(l_x, l_y, l_z);

	// ����� � ������� ��������� ��������� ������ (���������) ���������� ��������.
	std::cout << "\n";
	std::cout << "X-coordinate: " << l_vector.get_x() << "; Y-coordinate: " << l_vector.get_y() << "; Z-coordinate: " << l_vector.get_z();

	// ����� � ������� ����� �������.
	std::cout << "\n";
	std::cout << "Length of the Vector is: " << l_vector.get_length();
	std::cout << "\n";
}

/**
 * ������ � ������� �����.
 */
void homework_18_stack_class()
{
	// ���������� ����������.
	stack_class l_stack;
	bool l_exit = false;
	std::string l_str;
	
	std::cout << "Standart Stack class procedure.\n";

	// ����������� ���� � ������������.
	while (!l_exit)
	{
		// ������ ������������ ����� ��� �������� ��� ���������� � ����.
		std::cout << "Please enter Your Element Value or 'exit' for exit or 'pop' for popping last Element: ";
		std::cin >> l_str;

		// ���� ������� exit - ����� �� ���������.
		if (l_str == "exit")
		{
			l_exit = true;
		}
		// ���� ������� pop - ��������� ���������� �������� �����.
		else if (l_str == "pop")
		{
			l_stack.pop();
		}
		// � ����� ���� ������ �������� ����������� � ����.
		else
		{
			l_stack.push(l_str);
		}
	}
	std::cout << "Standart Stack class procedure ended.\n";
}

/**
 * ������ � ������� �����.
 */
void homework_18_stack_class_2()
{
	// ���������� ����������.
	own_stack_class l_stack;
	bool l_exit = false;
	std::string l_str;

	std::cout << "Own Stack class procedure.\n";

	// ����������� ���� � ������������.
	while (!l_exit)
	{
		// ������ ������������ ����� ��� �������� ��� ���������� � ����.
		std::cout << "Please enter Your Element INT Value or 'exit' for exit or 'pop' for popping last Element: ";
		std::cin >> l_str;

		// ���� ������� exit - ����� �� ���������.
		if (l_str == "exit")
		{
			l_exit = true;
		}
		// ���� ������� pop - ��������� ���������� �������� �����.
		else if (l_str == "pop")
		{
			l_stack.pop();
		}
		// � ����� ���� ������ �������� ����������� � ����.
		else
		{
			l_stack.push(std::stoi(l_str));
		}
	}
	std::cout << "Own Stack class procedure ended.\n";
}

/**
 * ������ � ������� T-�����.
 */
void homework_18_stack_class_3()
{
	bool l_exit = false;

	t_own_stack_class <std::string> l_stack;

	// ����������� ���� � ������������.
	while (!l_exit)
	{
		std::string l_str;
		// ������ ������������ ����� ��� �������� ��� ���������� � ����.
		std::cout << "Please enter Your Element Value or 'exit' for exit or 'pop' for popping last Element: ";
		std::cin >> l_str;

		// ���� ������� exit - ����� �� ���������.
		if (l_str == "exit")
		{
			l_exit = true;
		}
		// ���� ������� pop - ��������� ���������� �������� �����.
		else if (l_str == "pop")
		{
			l_stack.pop();
		}
		// � ����� ���� ������ �������� ����������� � ����.
		else
		{
			l_stack.push(l_str);
		}
	}
	std::cout << "Own T-Stack class procedure ended.\n";
}

/**
 * ������ � ������������� �������.
 */
void homework_19_class()
{
	animal_class* l_animal[3];

	l_animal[0] = new cow_class;
	l_animal[1] = new dog_class;
	l_animal[2] = new cat_class;

	for (int c = 0; c < 3; c++) l_animal[c]->voice();
}