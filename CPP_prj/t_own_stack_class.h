template <typename T>
class t_own_stack_class
{
private:
    T* stack; // ��������� �� ����
    int size; // ������ �����
public:
    // �����������
    t_own_stack_class()
    {
        stack = nullptr;
        size = 0;
    };

    // ����������
    ~t_own_stack_class()
    {
        delete[] stack;
    };

    /**
     * �������� ����� ������� � ����.
     * param value. �������� ������ ��������.
     */
    void push(const T value)
    {
        // ��������� ����.
        T* temp_stack = new T[++size];
        // ������� �� �������� ����� ����������� � �����.
        if (size > 1) {
            for (int c = 0; c < size - 1; c++)
            {
                temp_stack[c] = *(stack + c);
            }
        }
        // ���������� ������ �������� � ����� ������ �����.
        temp_stack[size - 1] = value;
        // ������� �������� ������ ����������� �����.
        delete[] stack;
        // ������� ��������� �� ����� ������� ������.
        stack = &temp_stack[0];
    };

    /**
     * ������� � ������� �������� �������� � ������� ��� �� �����.
     */
    void pop()
    {
        // ���� ���� �������������� ������, ��������� �������� ��������.
        if (size > 0) {
            // ���������� � ���������� �������� �������� ��������.
            T l_last_val = *(stack + size - 1);
            std::cout << "The Last Element is: " << l_last_val;

            // ��� ������� ��������� � ������ �������, ����������� �� � ����� ������� ������.
            if (size > 1) {
                T* temp_stack = new T[--size];
                for (int c = 0; c < size; c++)
                {
                    temp_stack[c] = *(stack + c);
                }
                // ������� ����������� �������� ������.
                delete[] stack;
                // ���� ���� �������� � ����� �����, ����������� ��������� ����� �� ����� ������� ������.
                if (size > 0) stack = &temp_stack[0];
            }
            // ���� ������� ���� �������, ��������� ������ �����, ��������� �����������, ��� ������.
            else if (size == 1)
            {
                delete[] stack;
                size--;
                stack = nullptr;
            }
        }
        // ��� ���������� ���������, ���������� �� ����.
        else
        {
            std::cout << "Stack is empty.";
        }
        std::cout << "\n";
    };
};