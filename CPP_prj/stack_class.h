class stack_class
{
  private:
    // ���������� ������� �����.
    std::stack <std::string> stack;
  public:
    // ������������.
    stack_class() {};
    // ����������.
    ~stack_class() {};

   /**
    * �������� ����� ������� � ����.
    * param p_val. �������� ������ ��������.
    */
    void push(std::string p_val)
    {
        stack.push(p_val);
    }

   /**
    * ������� � ������� �������� �������� � ������� ��� �� �����.
    */
    void pop()
    {
        // ���� ���� �� ������, ��������� � ������� ��������� �������.
        if (!stack.empty())
        {
            std::cout << "The Last Element is: " << stack.top();
            stack.pop();
        }
        // ���� ���� ������, ��������� � ������� ��������� �� ����.
        else
        {
            std::cout << "Stack is empty.";
        }
        std::cout << "\n";
    }
};

