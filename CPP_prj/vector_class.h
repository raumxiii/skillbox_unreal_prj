class vector_class
{
  private:
	  float x, y, z;
  public:
	  // ��������� ���������� X.
	  void set_x(float p_x)
	  {
		  x = p_x;
	  }

	  // ��������� ���������� Y.
	  void set_y(float p_y)
	  {
		  y = p_y;
	  }

	  // ��������� ���������� Z.
	  void set_z(float p_z)
	  {
		  z = p_z;
	  }

	  // ��������� ��������� XYZ.
	  void set_point(float p_x, float p_y, float p_z)
	  {
		  set_x(p_x);
		  set_y(p_y);
		  set_z(p_z);
	  }

	  // ��������� ���������� X.
	  float get_x()
	  {
		  return x;
	  }

	  // ��������� ���������� Y.
	  float get_y()
	  {
		  return y;
	  }

	  // ��������� ���������� Z.
	  float get_z()
	  {
		  return z;
	  }

	  // ��������� ����� �������.
	  float get_length()
	  {
		  return sqrt(pow(x, 2)  + pow(y, 2) +  pow(z, 2));
	  }
};

